package com.gbi.youth.youthapp;

import android.content.Context;
import android.util.AttributeSet;
import android.webkit.WebView;

/**
 * Created by Stanley on 09/10/2017.
 */

public class CustomizedWebView extends WebView {
    public CustomizedWebView(Context context) {
        super(context);
    }

    public CustomizedWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        MainActivity mainAct = (MainActivity)this.getContext();
        if(l == 0 && t == 0) {
            if(!mainAct.getSupportActionBar().isShowing()) {
                mainAct.getSupportActionBar().show();
            }
        } else {
            if (mainAct.getSupportActionBar().isShowing()) {
                mainAct.getSupportActionBar().hide();
            }
        }

        super.onScrollChanged(l, t, oldl, oldt);
    }
}
